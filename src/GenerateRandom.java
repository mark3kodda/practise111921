
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class GenerateRandom {

    int start;
    int end;
    public ArrayList<Integer> excludeList;

    //конструктор класса
    public GenerateRandom(int start, int end, ArrayList<Integer>excludeList) {
        this.start = start;
        this.end = end;
        this.excludeList = excludeList;
    }

    //геттеры сеттеры

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    //метод выдавающий рандом
    public int getNextRandom() throws IllegalArgumentException{
        Random rnd = new Random();
        int random = start + rnd.nextInt(end - start + 1 - excludeList.size());

        for (int ex : excludeList) {
            if (random < ex) {
                break;
            }
            random++;

        }
        excludeList.add(random);
        Collections.sort(excludeList);

        return random;
    }
}


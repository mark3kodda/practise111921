import java.util.ArrayList;
public class Main {

    public static void main(String[] args) {

        //1) Заполнить двумерный массив и вывести 7*7
        System.out.println("TASK 1");
        System.out.println("");

        String[][] arr1 = {
                {"0","0","0","0","0","0","0",},
                {"0","0","0","0","0","0","0",},
                {"0","0","0","0","0","0","0",},
                {"0","0","0","0","0","0","0",},
                {"0","0","0","0","0","0","0",},
                {"0","0","0","0","0","0","0",},
                {"0","0","0","0","0","0","0",}
        };
        for (int i = 0; i <arr1.length ; i++) {

            for (int j = 0; j <arr1[i].length ; j++) {
                System.out.print("| "+arr1[i][j]+" ");

            }
            System.out.print("|");
            System.out.println("");

        }

        /*

        2) Создать метод на вход которому приходит 3х мерный масссив тип int
        Метод находит самое маленькое число в масиве и самое большое
        Метод возвращает обьект с 2мя полями minValue, maxValue

         */
        System.out.println("");
        System.out.println("TASK 2");

        int [][][] numArr= {
                {
                        {100,14,5,2,7},
                        {12},
                        {162,-101,15},
                },
                {
                        {2,26,14,3},
                        {5,51,-101,66},
                        {16,3}
                }
        };

        System.out.println(MinMaxELemArray.findMinMaxElem(numArr));

        System.out.println();

        /*

        4) Используя рандом, заполнить массив из 20 элементов неповторяющимися числами

         */
        System.out.println("TASK 4");

        int [] arr4 = new int[20];

        ArrayList<Integer> excludeList = new ArrayList<Integer>();
        GenerateRandom searchPool = new GenerateRandom(1,30,excludeList);

        System.out.println();
        System.out.println("Array of unique number");

        for (int i = 0; i <arr4.length ; i++) {
            arr4[i]=searchPool.getNextRandom();
            System.out.print(arr4[i]+" ");
        }
        System.out.println("");
        System.out.println("");

        /*

        5) Дан массив целых чисел
        Все элементы, оканчивающиеся цифрой 4, уменьшить вдвое
        Все четные элементы заменить на их квадраты, а нечетные удвоить

         */
        System.out.println("TASK 5");


        int [] arr5 = {24,13,12,16,544,61,47,84,5};

        System.out.println("Given array");

        for (int i = 0; i <arr5.length ; i++) {
            System.out.print(arr5[i]+" ");
        }
        System.out.println("");

        char fourAsChar = '4';
        System.out.println();
        //       тут обрабатываем числа с 4кой в конце
        for (int i = 0; i <arr5.length ; i++) {

            //сейчас срабатывает на 4 в любом месте числа

            //String temp = String.valueOf(arr5[i]);
            //

            char [] checkIfEndedWithFour = String.valueOf(arr5[i]).toCharArray();


            if(checkIfEndedWithFour[checkIfEndedWithFour.length-1]==fourAsChar){
                //System.out.println(arr5[i] + " end with 4. Going down");
                //System.out.println(arr5[i]+" went to "+arr5[i]/2);
                arr5[i]=(arr5[i]/2);
            }//
        }


        //System.out.println();
        System.out.println("Temporal array");
        for (int i = 0; i <arr5.length ; i++) {
            System.out.print(arr5[i]+" ");
        }
        System.out.println();


        // тут обрабатываем условия на чётные нечётный

        for (int i = 0; i <arr5.length ; i++) {

            if(arr5[i]%2==0){
                //System.out.print(arr5[i] + "");
                arr5[i]=(int)Math.sqrt(arr5[i]);
                //System.out.println(" trasformed to "+arr5[i]);
            }else{
                arr5[i]=arr5[i]*2;
            }
        }

        System.out.println();
        System.out.println("Resulting array");

        for (int i = 0; i <arr5.length ; i++) {
            System.out.print(arr5[i]+" ");
        }

        System.out.println();

        System.out.println();

        /*

        3) В массиве одинаковое количество четных и нечетных чисел в произвольном порядке.
          Отсортировать массив следующим образом: четные числа расставить прямом порядке, нечетные в обратном, чередуя четные с нечетными числами.

        */
        System.out.println("TASK 3");
        System.out.println();
        int[] arr3 = {8,9,11,5,12,2,6,17};
        System.out.println("Given Array");
        for (int i = 0; i <arr3.length ; i++) {
            System.out.print(arr3[i] + " ");

        }
        int[] oddNum = new int[4];
        int[] evenNum = new int[4];
        int counter = 0;

        for (int i = 0; i <arr3.length ; i++) {
            if(arr3[i]%2==0){
                evenNum[counter]=arr3[i];
                counter++;
            }
        }
        counter=0;
        SortArrayBubble(evenNum);

        for (int i = 0; i <arr3.length ; i++) {
            if(arr3[i]%2!=0){
                oddNum[counter]=arr3[i];
                counter++;
            }
        }

        SortArrayBubble(oddNum);

        int [] oddNum2 = new int[oddNum.length];
        for (int i = 0; i <oddNum2.length ; i++) {
            oddNum2[i]=oddNum[3-i];
        }

        System.out.println();
        int counterEven=0;
        int counterOdd=0;

        for (int i = 0; i <arr3.length ; i++) {
            if(i%2==0){
                arr3[i] = evenNum[counterEven];
                counterEven++;
            }else{
                arr3[i] = oddNum2[counterOdd];
                counterOdd++;
            }

        }

        System.out.println("Resulted Array");

        for (int i = 0; i <arr3.length ; i++) {
            System.out.print(arr3[i]+" ");

        }

    }
    public static int [] SortArrayBubble(int [] a){

        boolean isSorted = false;
        int buf = 0;
        while(!isSorted){
            isSorted = true;
            for (int i = 0; i <a.length-1 ; i++) {
                if(a[i] > a[i+1]){
                    isSorted = false;
                    buf=a[i];
                    a[i] = a[i+1];
                    a[i+1] = buf;
                }
            }
        }
        return a;
    }

}

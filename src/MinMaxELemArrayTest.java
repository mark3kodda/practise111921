
class MinMaxELemArrayTest {

    @org.junit.jupiter.api.Test
    void findMinMaxElem() {

        int[][][] arrayToBeTested = {
                {
                        {100,14,5,2,7},
                        {12},
                        {162,-101,15},
                },
                {
                        {2,26,14,3},
                        {5,51,-101,66},
                        {16,3}
                }
        };
        String expected = "Minimal number is "+-101+"\nMax number is "+162;
        String actual = MinMaxELemArray.findMinMaxElem(arrayToBeTested);


    }
}
